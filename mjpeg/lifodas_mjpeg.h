#ifndef __LIFODAS_MJPEG_H__
#define __LIFODAS_MJPEG_H__

#include <stdint.h>

typedef struct __lifodas_rect_type lifodas_rect_type;

struct __lifodas_rect_type {
    size_t 
        x, // X starting position in pixels
        y, // Y starting position in pixels
        w, // width in pixels
        h; // height in pixels
};

#define LIFODAS_MJPEG_STREAM_SKIP            1 // Skip stream data

#define LIFODAS_MJPEG_BLOCK_FRAME_START      0 // Rect contains the full size of the bitmap

#define LIFODAS_MJPEG_FORMAT_MONOC8          1 // Monochrome 8bit bitmap
#define LIFODAS_MJPEG_FORMAT_RGB565          2 // RGB 16bit 565 
#define LIFODAS_MJPEG_FORMAT_RGB888          3 // RGB 24bit 888

#define LIFODAS_MJPEG_RESULT_SUCCESS         0 // Successfully completed
#define LIFODAS_MJPEG_RESULT_OUT_OF_MEM     -1 // Heap was too small to complete operation
#define LIFODAS_MJPEG_RESULT_BAD_FORMAT     -2 // Corrupt data
#define LIFODAS_MJPEG_RESULT_UNSUPPORTED    -3 // Unsupported format
#define LIFODAS_MJPEG_RESULT_ABORTED        -4 // Aborted by client
#define LIFODAS_MJPEG_RESULT_INVALID_ARGS   -5 // Invalid arguments passed to function
#define LIFODAS_MJPEG_RESULT_READ_ERROR     -6 // failed to read stream or block
#define LIFODAS_MJPEG_RESULT_WRITE_ERROR    -7 // failed to write stream or block
#define LIFODAS_MJPEG_RESULT_PANICK         -8 // Panick

/*
 *      ctx_                - context passed in the init function
 *      inSize_/outSize_    - Size of data to read or write to the stream
 *      inFlags_            - flags for the operation
 *      inData_/outData_    - Pixel/Stream data for the operation
 *      ioRect_             - rectangle of the operation (for frame start operations this will be the total size of the bitmap)
 */
typedef int __cdecl lifodas_mjpeg_rstream_callback_type (void *ctx_, void const ** outData_, size_t inSize_, size_t *outSize_, size_t inFlags_);    // stream read (for decoding)
typedef int __cdecl lifodas_mjpeg_wblock_callback_type  (void *ctx_, void const *  inData_, lifodas_rect_type const *inRect_, size_t inFlags_);     // block write (for decoding)

typedef int __cdecl lifodas_mjpeg_rblock_callback_type  (void *ctx_, void const ** outData_, lifodas_rect_type *ioRect_, size_t inFlags_);          // block read   (for encoding)
typedef int __cdecl lifodas_mjpeg_wstream_callback_type (void *ctx_, void const *  inData_, size_t inSize_, size_t *outSize_, size_t inFlags_);     // stream write (for encoding)

#ifdef __cplusplus
extern "C" {
#endif
    /**************************************************************************
     *                    DECODER DECLARATIONS                                *
     **************************************************************************/
    
    int lifodas_mjpeg_decoder_heap_init (
        void *heap_,                                    // heap pointer
        size_t hsize_,                                  // heap size
        lifodas_mjpeg_rstream_callback_type *rscbk_,    // stream read callback
        void *rctx_,                                    // stream read context
        lifodas_mjpeg_wblock_callback_type *wbcbk_,     // block write callback
        void *wctx_,                                    // block write context
        size_t flags_);                                 // flags
    
    int lifodas_mjpeg_decoder_extract_frame (           // decode one frame
        void *heap_);                                   // heap that was passed to the init function
    
    int lifodas_mjpeg_decoder_cleanup (
        void *heap_);
    
    /**************************************************************************
     *                    ENCODER DECLARATIONS                                *
     **************************************************************************/
    
    int lifodas_mjpeg_encoder_heap_init (
        void *heap_,                                    // heap pointer
        size_t hsize_,                                  // heap size
        lifodas_mjpeg_rblock_callback_type *rbcbk_,     // block read callback
        void *rctx_,                                    // block read context
        lifodas_mjpeg_wstream_callback_type *wscbk_,    // stream write callback
        void *wctx_,                                    // stream write context
        size_t flags_);                                 // flags
    
    int lifodas_mjpeg_encoder_pack_frame (              // encode frame
        void *heap_);                                   // heap that was passed to the init function
    
    int lifodas_mjpeg_encoder_cleanup (
        void *heap_);
 
#ifdef __cplusplus
}
#endif

#endif