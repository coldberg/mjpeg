#include <SDL.h>
#undef main
#pragma comment(lib, "SDL2.lib")

#include <cstdlib>
#include <cstdint>
#include <iostream>
#include <fstream>
#include <filesystem>
#include <vector>
#include <string>
#include <regex>
#include <iomanip>
#include <algorithm>

#include "lifodas_mjpeg.h"

namespace util {
    struct Loader {
        
        Loader (std::string const &in_):
            offset_ (0),
            buffer_ (load (in_))
        {}
    
        int __cdecl read (std::uint8_t const * (&outData_), size_t inSize_, size_t (&outSize_), size_t inFlags_) {            

            //std::cout << __FUNCSIG__ << "\n";

            if (offset_ >= buffer_.size ()) {
                return LIFODAS_MJPEG_RESULT_ABORTED;
            }
    
            auto sizeLeft_ = buffer_.size () - offset_;
            inSize_ = std::min (sizeLeft_, inSize_);
    
            &outSize_ ? outSize_ = inSize_ : 0 ;
            &outData_ ? outData_ = &buffer_ [offset_] : 0 ;
            
            offset_ += inSize_;
    
            return LIFODAS_MJPEG_RESULT_SUCCESS;
        }
    
    private:
    
        static std::string 
            replace_environment (std::string const &in) 
        {
            auto needle_ = std::regex ("\\%[^\\%]+\\%", std::regex_constants::optimize);
            auto bi = std::sregex_token_iterator (std::begin (in), std::end (in), needle_, {-1, 0});
            auto ei = std::sregex_token_iterator ();
            std::string out_ ;
        
            std::for_each (bi, ei, [&out_] (std::string const &in) {
                if (in.size () > 2 &&
                    *std::begin (in) == '%' && 
                    *std::rbegin (in) == '%') 
                {
                    auto name_ = std::getenv (
                        in.substr (1, in.size () - 2).c_str ());
                    out_.append (name_);
                    return;
                }
                out_.append (in); 
            });
            return out_;
        }
    
        static std::vector<std::uint8_t> 
            load (std::string const &in_) 
        {
            auto name_ = replace_environment (in_);
            std::cout << "Preloading : " << name_ << "...\n";
        
            auto size_ = std::streamsize (std::tr2::sys::file_size (
                std::tr2::sys::path (name_)));
        
            if (size_ < 1) {
                throw std::runtime_error (
                    name_ + ": File not found!");
            }
        
            auto buffer_ = std::vector<std::uint8_t> (std::uint32_t (size_));
            auto stream_ = std::ifstream (name_, std::ios::binary);
        
            std::cout << "\n";
            for (std::streamsize total_ = 0; total_ < size_;) {
                stream_.read (reinterpret_cast<char *> (&buffer_ [std::size_t (total_)]), (50*1024*1024));
                total_ += stream_.gcount () ;
                std::cout << std::setw (7) << std::setprecision (3)  << "Loaded " << ((100.0*total_)/size_) << "% \r";
            }
            std::cout << "\n";
            if (buffer_.size () != size_) {
                throw std::runtime_error (
                    name_ + ": Error reading file!");
            }
        
            return buffer_;
        }
    
        std::size_t offset_;
        std::vector<std::uint8_t> buffer_;
    };


    struct Painter {
        Painter (SDL_Surface *pSurface) :
            surface_ (pSurface)
        {}

        int __cdecl paint (std::uint8_t const *inData_, lifodas_rect_type const *inRect_, size_t inFlags_) {            
            std::cout << __FUNCSIG__ << "\n";
            return LIFODAS_MJPEG_RESULT_SUCCESS;
        }

    private:
        SDL_Surface *surface_;
    };


    template <typename OutType, typename InType>
        OutType method_ptr_cast (InType &&in_) {
            union {
                InType  p_in_;
                OutType p_out_;
            };
            p_in_ = std::forward<InType> (in_);
            return p_out_;
        }

}


int main (int, char **) try {
    static std::uint8_t heap_ [0x100000];

    SDL_Init (SDL_INIT_EVERYTHING);
    std::atexit (SDL_Quit);

    auto pWindow = SDL_CreateWindow ("Hello", 
        SDL_WINDOWPOS_CENTERED, 
        SDL_WINDOWPOS_CENTERED, 
        1280, 720, 
        SDL_WINDOW_SHOWN);
    auto pSurface = SDL_GetWindowSurface (pWindow);


    auto loader_ = util::Loader ("%DATA%/t.mjpeg");
    auto painter_ = util::Painter (pSurface);

    lifodas_mjpeg_decoder_heap_init (&heap_ [0], sizeof (heap_), 
        util::method_ptr_cast<lifodas_mjpeg_rstream_callback_type *> (&util::Loader::read), &loader_,
        util::method_ptr_cast<lifodas_mjpeg_wblock_callback_type *> (&util::Painter::paint), &painter_, 
        0);


    auto pEvent = SDL_Event {} ;
    for (;;) {
        if (SDL_PollEvent (&pEvent)) {
            if (pEvent.type == SDL_QUIT)
                break;
            continue;
        }
        lifodas_mjpeg_decoder_extract_frame (&heap_ [0]);
        SDL_UpdateWindowSurface (pWindow);
    }
    return 0;
}
catch (std::exception const &ex) {
    std::cout << ex.what () << "\n";
    std::cin.get () ;
}
