#include "lifodas_mjpeg_private.h"
#include <setjmp.h>
#include <string.h>
#include <stdio.h>
#include <assert.h>
/*******************************************************************
 * context structures
 *******************************************************************/

#define DUMP(f, v) printf (#v " = " f "\n", v)

#define FLAGS_ALLOC_ZERO    1
#define FLAGS_STREAM_SKIP   1

#pragma pack(push, 1)
typedef struct priv_common_context_type common_context_type;
typedef struct priv_decoder_context_type decoder_context_type;
typedef struct priv_encoder_context_type encoder_context_type;

struct priv_common_context_type {
    uint8_t                             *heap_ptr;
    size_t                              heap_size;
    size_t                              heap_top;
    size_t                              heap_bot;
    jmp_buf                             exception_jump_point;
    int                                 exception_return_code;
    void                                *rctx;
    void                                *wctx;
    union { 
        struct {
            lifodas_mjpeg_rstream_callback_type *rstream;
            lifodas_mjpeg_wblock_callback_type  *wblock;
        };
        struct {
            lifodas_mjpeg_rblock_callback_type  *rblock;
            lifodas_mjpeg_wstream_callback_type *wstream;
        };
    };
};

struct priv_decoder_context_type {
    common_context_type common_context;
    uint8_t const *buffer_ptr;
    uint8_t const *buffer_end;
    uint8_t qtable [4][64];
    uint16_t const *plut [2][2];
};

struct priv_encoder_context_type {
    common_context_type common_context;
};

#define CONTEXT(x) ((common_context_type *)(x))
#define DECODER(x) ((decoder_context_type *)(x))

#pragma pack(pop)

/*******************************************************************
 * utility functions                                               *
 *******************************************************************/

SMALL_PRIVATE uint16_t priv_u16 (uint16_t const w) {
    return (w >> 8) | (w << 8);
}

SMALL_PRIVATE uint8_t priv_u8clamp (int32_t const i) {
    return i <= 0x00 ? 0x00 : (i >= 255 ? 255 : (uint8_t)i);
}

SMALL_PRIVATE uint8_t priv_i8clamp (uint32_t const i) {
    return i <= -128 ? -128 : (i >= 127 ? 127 : (uint8_t)i);
}

SMALL_PRIVATE size_t priv_u8sum (uint8_t const * const w, size_t const n) {	
    register size_t i,s ;
    for (s = i = 0; i < n; ++i)
        s = s + w [i];
    return s;
}

SMALL_PRIVATE uint8_t *priv_build_sizes (uint8_t * const s, uint8_t const * const w) {
    register size_t i, j, k;
    for (k = 0, i = 0; i < 16; ++i)
        for (j = 0; j < w [i]; ++j, ++k)
            s [k] = (i + 1) & 0xff;
    return s;
}

SMALL_PRIVATE uint16_t *priv_build_codes (uint16_t * const h, uint8_t const * const s, size_t const n) {
    register size_t i, c;
    for (i = c = 0; i < n; c += (0x10000 >> s [i]), ++i)
        h [i] = c & 0xffff;
    return h;
}

/*******************************************************************
 * heap management, assert, i/o                                    *
 *******************************************************************/

SMALL_PRIVATE void priv_throw (void *heap_, int code_) {
    longjmp (CONTEXT (heap_)->exception_jump_point,
        CONTEXT (heap_)->exception_return_code = code_);
}

SMALL_PRIVATE void priv_assert (void *heap_, int cond_, int retc_) {
    if (cond_) return;      
    priv_throw (heap_, retc_);
}

SMALL_PRIVATE size_t priv_heap_size (void *heap_) {
    size_t used_ = CONTEXT (heap_)->heap_bot + CONTEXT (heap_)->heap_top;    
    return (CONTEXT (heap_)->heap_size > used_) ?
        CONTEXT (heap_)->heap_size - used_ : 0;
}

SMALL_PRIVATE void priv_heap_assert (void *heap_, size_t need_) {
    priv_assert (heap_, priv_heap_size (heap_) >= need_, 
        LIFODAS_MJPEG_RESULT_OUT_OF_MEM);
}

SMALL_PRIVATE void *priv_heap_alloc_top (void *heap_, size_t size_, int flags_) {    
    priv_heap_assert (heap_, size_);
    CONTEXT (heap_)->heap_top += size_;
    uint8_t *ptr_ = 
        CONTEXT (heap_)->heap_ptr +      
        CONTEXT (heap_)->heap_size -
        CONTEXT (heap_)->heap_top;
    if (flags_ & FLAGS_ALLOC_ZERO)
        memset (ptr_, 0, size_);

    return ptr_;
}

SMALL_PRIVATE void *priv_heap_alloc_bot (void *heap_, size_t size_, int flags_) {    
    priv_heap_assert (heap_, size_);
    uint8_t *ptr_ = 
        CONTEXT (heap_)->heap_ptr +
        CONTEXT (heap_)->heap_bot;
    CONTEXT (heap_)->heap_bot += size_;
    if (flags_ & FLAGS_ALLOC_ZERO)
        memset (ptr_, 0, size_);
    return ptr_;
}

SMALL_PRIVATE void priv_heap_reset_top (void *heap_) {
    CONTEXT(heap_)->heap_top = 0;
}

SMALL_PRIVATE void priv_heap_reset_bot (void *heap_) {
    CONTEXT(heap_)->heap_bot = 0;
}

SMALL_PRIVATE void const *priv_rstream_get (void *heap_, size_t inSize_, size_t *outSize_, size_t flags_) {
    void const *pData;
    int ret_ = CONTEXT(heap_)->rstream (CONTEXT(heap_)->rctx, 
        &pData, inSize_, outSize_, flags_);
    priv_assert (heap_, ret_ == LIFODAS_MJPEG_RESULT_SUCCESS, 
        ret_);    
    return pData;
}

SMALL_PRIVATE size_t priv_rstream_replenish (void *heap_) {
    decoder_context_type *pCtx_ = DECODER (heap_);
    size_t size;
    pCtx_->buffer_ptr = priv_rstream_get (heap_, CONST_BSIZE, &size, 0);
    pCtx_->buffer_end += size;
    return size;
}

SMALL_PRIVATE size_t priv_rstream_buffer_size (void *heap_) {
    return DECODER(heap_)->buffer_end - DECODER(heap_)->buffer_ptr;
}

SMALL_PRIVATE uint8_t priv_rstream_next_u8 (void *heap_) {
    if (priv_rstream_buffer_size (heap_) < 1 && priv_rstream_replenish (heap_) < 1)     
        priv_throw (heap_, LIFODAS_MJPEG_RESULT_READ_ERROR);
    return *DECODER(heap_)->buffer_ptr++;
}

SMALL_PRIVATE uint16_t priv_rstream_next_u16 (void *heap_) {
    register uint16_t r;
    if (priv_rstream_buffer_size (heap_) >= 2) {
        r = priv_u16 (*(uint16_t *)DECODER(heap_)->buffer_ptr);
        DECODER(heap_)->buffer_ptr += 2;         
    }
    else {
        r = priv_rstream_next_u8 (heap_);
        r = (r << 8) | priv_rstream_next_u8 (heap_);
    }
    return r;
}

SMALL_PRIVATE void priv_rstream_buffer_assert (void *heap_, size_t size) {
    priv_assert (heap_, priv_rstream_buffer_size (heap_) >= size, LIFODAS_MJPEG_RESULT_PANICK);
}

/*******************************************************************
 * body functions                                                  *
 *******************************************************************/

SMALL_PRIVATE uint16_t *priv_build_tables (
    void *heap_, 
    uint16_t const * const H, 
    uint8_t const * const S, 
    uint8_t const * const V,
    size_t const N)
{
    uint16_t *pr, *pe;
    size_t i = 0, j;
    uint32_t lsb, msb, xbs;
    uint16_t lsl, msl;

    pr = (uint16_t *)priv_heap_alloc_bot (heap_, 0x100*sizeof (*pr), FLAGS_ALLOC_ZERO);

    while (S [i] <= 8) {
        xbs = (1u << (8 - S [i]));
        msb = (H [i] >> 8);
        msl = (S [i] << 8) | (V [i]);
        for (j = 0; j < xbs; ++j)
            pr [msb + j] = msl;
        if (++i >= N)
            return pr;
    }

    while (S [i] <= 16) {
        xbs = 1u << (16 - S [i]);
        lsb = H [i] & 0xff;
        msb = (H [i] >> 8);
        lsl = (S [i] << 8) | (V [i]);

#if defined(LFD_JPEG_DEBUG)
        if ((pr [msb] & 0xFF00) && !(pr [msb] & 0x8000)) {			
            __debugbreak ();
        }
#endif		
        if (pr [msb] & 0x8000) {
            pe = &pr [pr [msb] & 0x7FFF];
        }
        else {
            pe = (uint16_t *)priv_heap_alloc_bot (heap_, 256*sizeof (*pe), FLAGS_ALLOC_ZERO);			
            pr [msb] = (0x8000 | (pe - pr)) & 0xFFFF;
        }

        for (j = 0; j < xbs; ++j)
            pe [lsb + j] = lsl;

        if (++i >= N)
            return pr;
    }

    while (i < N) {
        ++i;
    }

    return pr;

}


LARGE_PRIVATE void prvi_fetch_dqt (void *heap_, size_t size_) { 
    decoder_context_type *pCtx_ = DECODER(heap_);
    uint8_t Qk;
    while (size_ > 0) {
        Qk = (*pCtx_->buffer_ptr) >> 4;
        memcpy (&pCtx_->qtable [Qk], &pCtx_->buffer_ptr [1], 64);        
        pCtx_->buffer_ptr += 65;
        size_ -= 65;
    }
}

LARGE_PRIVATE void priv_fetch_dht (void *heap_, size_t size_) {
    decoder_context_type *pCtx_ = DECODER(heap_);
    uint8_t Tc, Th, *pVal, *pSize;
    uint16_t Ls, *pCode, *pLut;

    while (size_ > 0) {       
        Tc = *pCtx_->buffer_ptr >> 4;
        Th = *pCtx_->buffer_ptr & 0xf;
        priv_assert (heap_, Tc <= 1 && Th <= 1, LIFODAS_MJPEG_RESULT_UNSUPPORTED);
        Ls = priv_u8sum (pCtx_->buffer_ptr+1, 16);
        pVal = priv_heap_alloc_top (heap_, Ls*4, 0);
        pSize = pVal + Ls;    
        pCode = (uint16_t *)(pSize + Ls);
        memcpy (pVal, pCtx_->buffer_ptr+17, Ls);
        priv_build_sizes (pSize, pCtx_->buffer_ptr+1);
        priv_build_codes (pCode, pSize, Ls);
        pLut = priv_build_tables (heap_, pCode, pSize, pVal, Ls);
        priv_heap_reset_top (heap_);
        pCtx_->plut [Tc][Th] = pLut;
        pCtx_->buffer_ptr += (17 + Ls);
        size_ -= (17 + Ls);        
    }
    
}


/*******************************************************************
 * decoder body                                                    *
 *******************************************************************/

int lifodas_mjpeg_decoder_heap_init (
    void *heap_,                                    // heap pointer
    size_t hsize_,                                  // heap size
    lifodas_mjpeg_rstream_callback_type *rscbk_,    // stream read callback
    void *rctx_,                                    // stream read context
    lifodas_mjpeg_wblock_callback_type *wbcbk_,     // block write callback
    void *wctx_,                                    // block write context
    size_t flags_)                                  // flags
{
    if (hsize_ <= sizeof (decoder_context_type))
        return LIFODAS_MJPEG_RESULT_INVALID_ARGS;

    common_context_type *pCtx_ = CONTEXT (heap_);    

    memset (heap_, 0, sizeof (decoder_context_type));

    pCtx_->heap_ptr = (uint8_t *)heap_ + sizeof (decoder_context_type);
    pCtx_->heap_size = hsize_ - sizeof (decoder_context_type);    
    pCtx_->rstream = rscbk_;
    pCtx_->wblock = wbcbk_;
    pCtx_->rctx = rctx_;
    pCtx_->wctx = wctx_;
    
    return LIFODAS_MJPEG_RESULT_SUCCESS;
}

int lifodas_mjpeg_decoder_extract_frame (           // decode one frame
    void *heap_)                                    // heap that was passed to the init function
{
    decoder_context_type *pCtx_ = DECODER (heap_);

    if (setjmp (pCtx_->common_context.exception_jump_point)) 
        return pCtx_->common_context.exception_return_code;
    
    uint16_t marker, length ;

    for (;;) {		
        length = 0;
        marker = priv_rstream_next_u16 (heap_);
        if (marker < 0xff00) 
            priv_throw (heap_, LIFODAS_MJPEG_RESULT_UNSUPPORTED);
        marker = marker & 0xff;		
        if (marker < 0xD0 || marker >= 0xDA)
            length = priv_rstream_next_u16 (heap_) - sizeof (length);
        priv_rstream_buffer_assert (heap_, length);

        switch (marker) {
            case 0xDB: // Define quantization tables
                prvi_fetch_dqt (heap_, length);
                continue;

            case 0xC4: // Define huffman table                
                priv_fetch_dht (heap_, length);
                continue;

            case 0xC0: // Start of frame (Baseline DCT)
                __debugbreak () ;                
                continue;

            case 0xDA: // Start of scan data								
                //__jpeg_fetch_sos (&context, length);					
                //__jpeg_decode_scan (&context);
                //
                return LIFODAS_MJPEG_RESULT_SUCCESS;

            case 0xD8: // Sart of image                
                continue;

            case 0xD9: // End of image
                //__jpeg_exit (&context, 0);
                continue;

            case 0xFE: // Comment
            case 0xE0: // JFIF/JFXX header
            default:
                DECODER(heap_)->buffer_ptr += length;
                continue;

            case 0xC1: // Exteded sequential DCT (Huffman)
            case 0xC2: // Progressive DCT (Huffman)
            case 0xC3: // Lossless (Huffman)
            case 0xC5: // Differential sequential DCT (Huffman)
            case 0xC6: // Differential progressive DCT (Huffman)
            case 0xC7: // Differential lossless (Huffman)
            case 0xC8: // Reserved for extensions (Arithmetic)
            case 0xC9: // Extended sequential DCT (Arithmetic)
            case 0xCA: // Progressive DCT (Arithmetic)
            case 0xCB: // Lossless sequential (Aritmetic)
            case 0xCD: // Differential sequential DCT (Arithmetic)
            case 0xCE: // Differential progressive DCT (Arithmetic)
            case 0xCF: // Differential lossless sequential (Arithmetic)
            case 0xCC: // Define arithmetic coding conditions
                //__jpeg_exit (&context, LFD_JPEG_ERROR_BAD_FORMAT);
                continue;
        }

    }

    
    return LIFODAS_MJPEG_RESULT_SUCCESS;
}

int lifodas_mjpeg_decoder_cleanup (
    void *heap_)
{
    return LIFODAS_MJPEG_RESULT_SUCCESS;
}
