#ifndef __LIFODAS_MJPEG_PRIVATE_H__
#define __LIFODAS_MJPEG_PRIVATE_H__

#include "lifodas_mjpeg.h"


#define CONST_BITS    16            
#define CONST_HALF    32768			// 2^(B - 1)
#define CONST_C1      32138			// cos (1*PI/16)/2
#define CONST_C2      30274			// cos (2*PI/16)/2
#define CONST_C3      27246			// cos (3*PI/16)/2		
#define CONST_C4      23170			// cos (4*PI/16)/2, cos (0*PI/16)/(2*sqrt(2)) 									 
#define CONST_C5      18205			// cos (5*PI/16)/2
#define CONST_C7      6393			// cos (7*PI/16)/2
#define CONST_C6      12540			// cos (6*PI/16)/2
        
#define CONST_A0      91881         // yuv conversion consts
#define CONST_A1      22553         // yuv conversion consts
#define CONST_A2      46801         // yuv conversion consts
#define CONST_A3      116129        // yuv conversion consts

#define CONST_BSIZE   1024          // buffer size


#if defined(_MSC_VER)
#define FORCE_INLINE __forceinline 
#endif

#if defined(__GNUC__)
#define FORCE_INLINE __attribute__((always_inlie))
#endif

#define SMALL_PRIVATE FORCE_INLINE
#define LARGE_PRIVATE static
#define LARGE_PUBLIC  extern

#endif